class profile::base {
  class { selinux:
    mode => 'disabled',
    type => 'targeted',
  }
  user {'admin':
    ensure => present
  }
  service { 'firewalld':
    ensure => stopped,
    enable => false,
  }
}